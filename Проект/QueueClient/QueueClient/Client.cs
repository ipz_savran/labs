﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;
using StaticClasses;

namespace QueueClient
{
    class Client
    {
        public static Client instance;

        public Data data = new Data();
        public StaticClasses.Client clientQueue { get; set; }
        public string cabinetName { get; set; }

        private TcpClient client;
        private NetworkStream stream;
        private BinaryFormatter bf = new BinaryFormatter();

        public Client()
        {
            try
            {
                client = new TcpClient();
                client.Connect("127.0.0.1", 8000);
                stream = client.GetStream();
            }
            catch
            {
                MessageBox.Show("Неможливо підключитись до сервера");
                Application.Current.Shutdown();
            }
        }
        ~Client()
        {
            stream.Close();
            client.Close();
        }
        public static void Create()
        {
            instance = new Client();
        }

        public void SendCode(byte code)
        {
            try
            {
                stream.WriteByte(code);

                switch (code)
                {
                    case 0:
                        stream.Close();
                        client.Close();
                        break;
                    case 1:
                        clientQueue = (StaticClasses.Client)bf.Deserialize(stream);
                        break;
                    case 2:
                        data = (Data)bf.Deserialize(stream);
                        break;
                    case 3:
                        bf.Serialize(stream, clientQueue);
                        bf.Serialize(stream, cabinetName);

                        data = (Data)bf.Deserialize(stream);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Сервер недоступний");
                stream.Close();
                client.Close();
                Application.Current.Shutdown();
            }
        }
    }
}
