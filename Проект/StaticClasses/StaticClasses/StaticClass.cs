﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticClasses
{
    [Serializable]
    public class Data
    {
        public List<Cabinet> cabinets = new List<Cabinet>();
        public List<string> notifications = new List<string>();
    }
    [Serializable]
    public class Cabinet
    {
        public string name { get; set; }
        public List<Client> connected { get; set; }
    }
    [Serializable]
    public class Client
    {
        public string name { get; set; }
        public float timeToEnd { get; set; }
    }
}
