﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using StaticClasses;

namespace QueueServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.Create();

            IPAddress ip = IPAddress.Parse("127.0.0.1");
            int port = 8000;
            Console.WriteLine("Ip: {0}\tPort: {1}", ip, port);
            TcpListener server = new TcpListener(ip, port);

            server.Start();
            Timer timer = new Timer(TimerCallback, null, 0, 100);
            int clientIndex = 0;
            while (true)
            {
                Console.WriteLine("Очiкування клiєнтiв...");

                TcpClient client = server.AcceptTcpClient();
                Task.Run(() => NewClient(client, ++clientIndex));
            }
        }
        public static void TimerCallback(Object o)
        {
            Database.instance.data.cabinets.ForEach(x =>
            {
                var toRemove = new List<Client>();
                x.connected.ForEach(y =>
                {
                    y.timeToEnd -= 0.1f;

                    if (y.timeToEnd <= 0)
                        toRemove.Add(y);
                });
                foreach (var y in toRemove)
                {
                    x.connected.Remove(y);
                    Database.instance.data.notifications.Insert(0, string.Format("{0} завершив роботу та звільнив чергу", y.name));
                }
            });
        }
        public static void NewClient(TcpClient client, int id)
        {
            NetworkStream stream = client.GetStream();

            Console.WriteLine("\tКлiєнт {0} пiдключився", id);
            BinaryFormatter bf = new BinaryFormatter();
            while (client.Connected)
            {
                if (stream.DataAvailable)
                {
                    int code = stream.ReadByte();
                    Console.WriteLine("\tКлiєнт {0}: Отриманий код {1}", id, code);
                    switch (code)
                    {
                        case 0:
                            Console.WriteLine("\tКлiєнт {0} вiключився", id);
                            stream.Close();
                            client.Close();
                            return;
                        case 1:
                            bf.Serialize(stream, new Client() { name = string.Format("Клієнт: {0}", id) });
                            break;
                        case 2:
                            bf.Serialize(stream, Database.instance.data);
                            break;
                        case 3:
                            Client c = (Client)bf.Deserialize(stream);
                            string cabinetName = (string)bf.Deserialize(stream);

                            if (Database.instance.data.cabinets.Find(x => x.name == cabinetName).connected.Find(x => x.name == c.name) == null)
                            {
                                for (int i = 0; i < Database.instance.data.cabinets.Count; i++)
                                {
                                    if (Database.instance.data.cabinets[i].connected.Find(x => x.name == c.name) != null)
                                    {
                                        for (int j = Database.instance.data.cabinets[i].connected.IndexOf(Database.instance.data.cabinets[i].connected.Find(x => x.name == c.name));
                                            j < Database.instance.data.cabinets[i].connected.Count; j++)
                                        {
                                            if (j > 1)
                                                Database.instance.data.cabinets[i].connected[j].timeToEnd = j * 10;
                                            else if (j == 1)
                                                Database.instance.data.cabinets[i].connected[j].timeToEnd = 10;
                                        }
                                        Database.instance.data.cabinets[i].connected.Remove(Database.instance.data.cabinets[i].connected.Find(x => x.name == c.name));
                                    }
                                }
                                try
                                {
                                    c.timeToEnd = Database.instance.data.cabinets.Find(x => x.name == cabinetName).connected.First().timeToEnd;
                                }
                                catch { }
                                if (Database.instance.data.cabinets.Find(x => x.name == cabinetName).connected.Count <= 0)
                                    c.timeToEnd += 10f;
                                else
                                    c.timeToEnd += (Database.instance.data.cabinets.Find(x => x.name == cabinetName).connected.Count) * 10f;
                                Database.instance.data.cabinets.Find(x => x.name == cabinetName).connected.Add(c);
                                Database.instance.data.notifications.Insert(0, string.Format("{0} зайняв чергу в кабінеті {1}", c.name, cabinetName));
                            }
                            bf.Serialize(stream, Database.instance.data);
                            break;
                    }
                }
                Thread.Sleep(100);
            }
        }
    }
}
