﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using StaticClasses;

namespace QueueServer
{
    class Database
    {
        public static Database instance;

        public Data data = new Data();

        public static void Create()
        {
            instance = new Database();
        }

        public Database()
        {
            Load();
            //Save();
        }

        public void Load()
        {
            try
            {
                FileStream file = File.Open(@"./Database.xml", FileMode.OpenOrCreate);
                XmlSerializer xml = new XmlSerializer(typeof(Data));
                data = (Data)xml.Deserialize(file);
                file.Close();
            }
            catch { try { Save(); } catch { } }
        }

        public void Save()
        {
            if (File.Exists(@"./Database.xml"))
                File.Delete(@"./Database.xml");

            FileStream file = File.Open(@"./Database.xml", FileMode.OpenOrCreate);

            List<Cabinet> cabinets = new List<Cabinet>();
            for (int i = 0; i < 2; i++)
            {
                cabinets.Add(new Cabinet() { name = (i + 1).ToString(), connected = new List<Client>() });
            }
            data.cabinets = cabinets;

            var serializer = new XmlSerializer(typeof(Data));
            using (var xw = XmlWriter.Create(file, new XmlWriterSettings() { NewLineOnAttributes = true, Indent = true }))
            {
                serializer.Serialize(xw, data);
            }

            file.Close();
        }
    }
}